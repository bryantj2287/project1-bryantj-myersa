# Mini - Golf Project
# Team Member's Alex Myers, Justin Bryant
from vector2 import vector2
from particle import Circle
from math import sqrt
from particle import Wall
from polygon import Polygon

class Contact:
    def __init__(self, obj1, obj2, restitution, normal, penetration):
        self.obj1 = obj1
        self.obj2 = obj2
        self.restitution = restitution
        self.normal = normal
        self.penetration = penetration
        
    def resolve(self):
        self.resolve_velocity()
        self.resolve_penetration()
        
    def resolve_penetration(self):
        if self.penetration > 0:
            total_invmass = self.obj1.invmass + self.obj2.invmass
            nudge = self.penetration/total_invmass
            self.obj1.pos += (self.obj1.invmass*nudge)*self.normal
            self.obj2.pos -= (self.obj2.invmass*nudge)*self.normal

    def resolve_velocity(self):
        sep_vel = (self.obj1.vel - self.obj2.vel)*self.normal
        if sep_vel < 0:
            target_sep_vel = -self.restitution*sep_vel
            delta_vel = target_sep_vel - sep_vel
            total_invmass = self.obj1.invmass + self.obj2.invmass
            if total_invmass > 0:
                impulse = delta_vel/total_invmass
                self.obj1.vel += (self.obj1.invmass*impulse)*self.normal
                self.obj2.vel -= (self.obj2.invmass*impulse)*self.normal

class ContactGenerator:
    def __init__(self, objects, restitution=0):
        self.objects = objects
        self.restitution = restitution
        for o in objects:
            o.contacts.append(self)
        self.contacts = []
    
    def remove(self, obj):
        self.objects.remove(obj)
        
    def contact_all(self):
        self.contacts.clear()
        # Loop through all pairs once
        for i in range(1, len(self.objects)):
            obj1 = self.objects[i]
            for j in range(i):
                obj2 = self.objects[j]
                if isinstance(obj1, Circle) and isinstance(obj2, Circle):
                    self.contact_circle_circle(obj1, obj2)
                elif isinstance(obj1, Circle) and isinstance(obj2, Wall):
                    self.contact_circle_wall(obj1,obj2)
                elif isinstance(obj1, Wall) and isinstance(obj2, Circle):
                    self.contact_circle_wall(obj2,obj1)
                elif isinstance(obj1, Wall) and isinstance(obj2, Wall):
                    pass
                elif isinstance(obj1, Polygon) and isinstance(obj2, Circle):
                    self.contact_circle_polygon(obj2,obj1)
                elif isinstance(obj1, Circle) and isinstance(obj2, Polygon):
                    self.contact_circle_polygon(obj1,obj2)
                elif isinstance(obj1, Polygon) and isinstance(obj2, Polygon):
                    pass
                else:
                    print("Warning! ContactGenerator not implemented between ",
                          type(obj1)," and ", type(obj2),".")
        return self.contacts

    def contact_circle_circle(self, obj1, obj2):
        r = obj1.pos - obj2.pos
        rmag2 = r.mag2()
        R = obj1.radius + obj2.radius
        if rmag2 < R*R:
            rmag = sqrt(rmag2)
            penetration = R - rmag
            normal = r/rmag
            self.contacts.append(Contact(obj1, obj2, self.restitution, normal, penetration))
            
    def contact_circle_wall(self, circle, wall):
        penetration = (wall.pos - circle.pos) * wall.normal.hat() + circle.radius
        if penetration > 0:
            self.contacts.append(Contact(circle,wall, self.restitution, wall.normal, penetration))
            
    def contact_circle_polygon(self, circle, polygon):
        polygon_contacts = []
        is_collision = True
        for w in polygon.walls: #Loop through every wall on the polygon
            penetration = (w.pos - circle.pos) * w.normal.hat() + circle.radius #Find the penetration between the circle and the wall
            #Use the penetration to create and save a collision
            polygon_contacts.append(Contact(circle,w,self.restitution, w.normal, (w.pos - circle.pos) * w.normal.hat() + circle.radius))
            if penetration <= 0: #If any wall is not colliding, then there is definitely no collision
                is_collision = False
        
        if is_collision:
            shortest_point = polygon.points[0]
            for p in polygon.points: # Loop through every point
                #Find which point is the closest to the circle
                if sqrt(pow(p.y - circle.pos.y, 2) + pow(p.x - circle.pos.x, 2)) < sqrt(pow(shortest_point.y - circle.pos.y, 2) + pow(shortest_point.x - circle.pos.x, 2)):
                    shortest_point = p
            
            #Create a temporary wall on the side of the circle facing the closest point
            temp_normal = (shortest_point - circle.pos).hat()
            temp_wall = Wall(0,circle.pos + (circle.radius * temp_normal), vector2(0,0), vector2(0,0), temp_normal, (0,0,0), 1)
            
            point_wall = polygon.points[0]
            num = 0
            wall_num = 0
            #Find which point has the greatest penetration with the temporary wall
            while num < len(polygon.points):
                if (temp_wall.pos - polygon.points[num]) * temp_wall.normal.hat() > (temp_wall.pos - point_wall) * temp_wall.normal.hat():
                    point_wall = polygon.points[num]
                    wall_num = int(num)
                num += 1
               
            if polygon_contacts[wall_num].penetration > 0: #If the penetration of the wall connected to the found wall is more than 0  
                #Find the wall with the least penetration
                best_wall = polygon_contacts[0]
                for c in polygon_contacts:
                    if c.penetration < best_wall.penetration:
                        best_wall = c
                self.contacts.append(best_wall) #Use the contact of the best wall
                #self.contacts.append(polygon_contacts[wall_num]) #This does some weird stuff.  I'm keeping it as an example.
                        
            
            
            