# Mini - Golf Project
# Team Member's Alex Myers, Justin Bryant
from particle import Particle
from particle import Wall
from vector2 import vector2
from math import sqrt
import pygame

class Polygon(Particle):
    def __init__(self, points):
        mass = 0
        pos = vector2(0,0)
        vel = vector2(0,0)
        gravity = vector2(0,0)
        Particle.__init__(self,mass,pos,vel,gravity)
        self.points = points
        self.walls = []
        
        num = 0
        while num < len(self.points): 
            self.walls.append(Wall(0, 
                                   (self.points[num - 1]+self.points[num])/2, #Position is the midpoint of the two points
                                   vector2(0,0), 
                                   vector2(0,0), 
                                   -1 * (self.points[num - 1] - self.points[num]).perp().hat(), #Normal is the vector perpendicular to the points 
                                   (255,255,255), 
                                   sqrt(pow(self.points[num].y - self.points[num - 1].y, 2) + pow(self.points[num].x - self.points[num - 1].x, 2)) / 2)) #Length is half the distance between the two points
            num += 1
        
    def draw(self, screen):
        pygame.draw.polygon(screen, (0,0,255), self.points)
        for w in self.walls:
            w.draw(screen)