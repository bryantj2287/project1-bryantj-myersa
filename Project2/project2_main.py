# Mini - Golf Project
# Team Member's Alex Myers, Justin Bryant
# File contains main method for pygame
import pygame
from vector2 import vector2
import math
from polygon import Polygon
from particle import Circle
import contact

def calcForce(vec1, vec2):
    # Return the length between two vectors
    # print(math.sqrt((math.pow(vec2.x-vec1.x,2))+(math.pow(vec2.y-vec1.y,2))))
    return math.sqrt((math.pow(vec2.x-vec1.x,2))+(math.pow(vec2.y-vec1.y,2)))
def direction(vec1, vec2):
    x = vec2.x - vec1.x
    y = vec2.y - vec1.y
    return vector2(x, y)
def point_in_circle(x, y, vec, radius):
    xRangeUp = x + radius
    xRangeDown = x - radius
    yRangeUp = y - radius
    yRangeDown = y + radius

    if (vec.x > xRangeDown and vec.x < xRangeUp):
        if (vec.y < yRangeDown and vec.y > yRangeUp):
            print("Function works")
            return True
    
    return False

def calcAngle(vec1, vec2, debug):

    x = (vec2.x - vec1.x)
    y = (vec2.y - vec1.y)
    #m = (vec2.y - vec1.y)/(vec2.x - vec1.x)
    rads = math.atan2(y, x)
    if (x == 0):
        if y > 0:
            rads = math.pi / 2
        else:
            rads = (3 * math.pi) / 2
    else:
        rads = math.atan(y/x)
    if debug:
        print("Result of standard atan")
        print(math.atan(y/x))
        print("Result of math.atan2")
        print(math.atan2(y, x))
        print("The control result is")
        print(math.atan2(4, 4))
    

    return rads

def main():
    # Initialize game vars
    pygame.init()
    screen_width = 1920
    screen_height = 1080
    screen1 = pygame.display.set_mode([screen_width,screen_height + 250])
    clock = pygame.time.Clock()
    done = False

    # Colors
    c_white = (255, 255, 255)
    c_black = (0, 0, 0)
    c_red = (255, 0, 0)
    c_green = (0, 255, 0)
    c_blue = (0, 0, 255)
    c_orange = (255, 127, 80)
    c_purple = (255, 0, 255)
    c_sandtrap = (225, 237, 177)
    c_background = (97, 181, 85)
    c_hill = (97, 165, 112)
    c_arrowColor = c_white;

    # Add fonts
    pygame.font.init()
    mainFont = pygame.font.SysFont("Comic Sans MS", 60)
    
    stroke = 0
    win = False

    # Create surfaces to draw text onto
    txtSurface1 = mainFont.render("Stroke: " + str(stroke), False, (0, 0, 0))

    


    # Player variables
    mPos = vector2(0, 0)
    mPos2 = vector2(0, 0)
    xPos, yPos = 0, 0
    unit = 50

    vert4 = vector2(0,0)
    # Set default vertices
    vert1 = vector2(mPos.x - 45, mPos.y - 50)
    vert2 = vector2(mPos.x - 45, mPos.y - 100)
    vert3 = vector2(vert4.x, vert4.y + 50)
    vert4 = vector2(mPos.x - 45, mPos.y + 50)

    originPos = vector2(350, 350)
    forceMagnitude = 0

    # BALL
    ball = Circle(1, vector2(350, 350), vector2(0, 0), vector2(0, 0), 25, c_purple)
    launchReady = False
    launchForceReduction = 0.5
    drag = 1.01
    maxLength = 400

    # HOLE
    hole = Circle(1, vector2(1500, 500), vector2(0, 0), vector2(0, 0), 30, c_blue)

    # Traps
    trap1 = Circle(1, vector2(700, 700), vector2(0, 0), vector2(0, 0), 150, c_sandtrap)
    trap2 = Circle(1, vector2(1500, 500), vector2(0, 0), vector2(0, 0), 175, c_hill)
    trap3 = Circle(1, vector2(1000, 650), vector2(0, 0), vector2(0, 0), 120, c_sandtrap)
    trap4 = Circle(1, vector2(1300, 150), vector2(0,0), vector2(0,0), 120, c_sandtrap)

    #POLYGON
    objects = []
    contact_generators = []
    objects.append(Polygon([vector2(0,0), vector2(1920, 0), vector2(0, 1), vector2(1920, 1)]))
    objects.append(Polygon([vector2(0,0), vector2(0, 1080), vector2(1, 1080), vector2(1, 0)]))
    objects.append(Polygon([vector2(0, 1080), vector2(1920, 1080), vector2(1920, 1079), vector2(0, 1079)]))
    objects.append(Polygon([vector2(1920, 1080), vector2(1920, 0), vector2(1919, 0), vector2(1919, 1080)]))
    objects.append(Polygon([vector2(200, 0), vector2(200, 350), vector2(450, 350)]))
    objects.append(Polygon([vector2(200, 750), vector2(200, 950), vector2(400, 950), vector2(400, 750)]))
    objects.append(Polygon([vector2(100, 1040), vector2(1800, 1040), vector2(1800, 1000), vector2(100, 1000)]))
    objects.append(Polygon([vector2(700, 200), vector2(700, 400), vector2(1000, 400), vector2(1000, 200)]))
    objects.append(Polygon([vector2(1700, 0), vector2(1900, 0), vector2(1900, 400)]))
    objects.append(ball)
    contact_generators.append(contact.ContactGenerator(objects, 0.3))

    # Basic Game Loop
    while not done:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
            elif event.type == pygame.MOUSEMOTION:
                if forceMagnitude < maxLength:
                    xPos, yPos = pygame.mouse.get_pos()
                    mPos.x = xPos
                    mPos.y = yPos
                else:
                    xPos, yPos = pygame.mouse.get_pos()
                    mPos2.x = xPos
                    mPos2.y = yPos
                if forceMagnitude < maxLength:
                    forceMagnitude = calcForce(ball.pos, mPos)
                else:
                    forceMagnitude = calcForce(ball.pos, mPos2)
                #print(forceDirection)
            elif event.type == pygame.MOUSEBUTTONUP:
                if launchReady:
                    launchReady = False
                    # Perform the launch
                    ball.vel = direction(ball.pos, mPos).hat() * calcForce(ball.pos, mPos)
                    stroke += 1
                    txtSurface1 = mainFont.render("Stroke: " + str(stroke), False, (0, 0, 0))
            elif event.type == pygame.MOUSEBUTTONDOWN:
                # Set the arrow color to white to make it invisible
                c_arrowColor = c_blue
                forceMagnitude = -1          
                #empty = calcAngle(originPos, mPos, True)
                if point_in_circle(ball.pos.x, ball.pos.y, mPos, 25):
                    if (ball.vel.x == 0 and ball.vel.y == 0):
                        print("Ready")
                        launchReady = True
        # mPos.x = originPos.x + (unit*3)
        # mPos.y = originPos.y  - (unit * 3)
           

        # Always update mPos
        # xPos, yPos = pygame.mouse.get_pos()
        # mPos.x = xPos
        # mPos.y = yPos
        # Update the force arrow
        screen1.fill(c_background)
        
        vert1 = vector2(mPos.x - unit, mPos.y - unit)
        vert2 = vector2(mPos.x - unit, mPos.y - (unit * 2))
        vert3 = vector2(vert4.x, vert4.y + unit)
        vert4 = vector2(mPos.x - unit, mPos.y + unit)
        
        # Return the current radian rotation
        radians = -math.pi / 4

        # Rotate vertices based on angle
        vert1 = vert1.rotated(radians)
        vert2 = vert2.rotated(radians)
        vert3 = vert3.rotated(radians)
        vert4 = vert4.rotated(radians)

        # Draws
        
        screen1.blit(txtSurface1, vector2(500, 50))
        trap2.draw(screen1)
        hole.draw(screen1)
        trap1.draw(screen1)
        trap3.draw(screen1)
        trap4.draw(screen1)
        if win:
            textSurface2 = mainFont.render("You Win with a score of " + str(stroke), False, (0, 0, 0))
            screen1.blit(textSurface2, vector2(810, 540))
        

        # Take in the user input when the ball is ready to launch
        if launchReady:
            # The arrow draws should go here
            pygame.draw.line(screen1, c_arrowColor, ball.pos, mPos, 5)
        

        # Apply drag to ball
        if (ball.vel.x != 0 or ball.vel.y != 0):
            ball.vel.x /= drag
            ball.vel.y /= drag
            if (ball.vel.x < 1 and ball.vel.x > -1):
                ball.vel.x = 0
            if (ball.vel.y < 1 and ball.vel.y > -1):
                ball.vel.y = 0

        # Applly major drag when in the hole or in traps
        if point_in_circle(hole.pos.x, hole.pos.y, ball.pos, 30):
            drag = 1.25
            # Check for the win condition
            if (ball.vel.x == 0 and ball.vel.y == 0):
                ball.pos = hole.pos
                win = True
        # Apply Sandtrap Drag
        elif point_in_circle(trap1.pos.x, trap1.pos.y, ball.pos, 150):
            drag = 1.15
         # Apply Sandtrap Drag
        elif point_in_circle(trap3.pos.x, trap3.pos.y, ball.pos, 120):
            drag = 1.15
         # Apply Sandtrap Drag
        elif point_in_circle(trap4.pos.x, trap4.pos.y, ball.pos, 120):
            drag = 1.15
        # Apply hill Drag
        elif point_in_circle(trap2.pos.x, trap2.pos.y, ball.pos, 175):
            # Apply opposite velocity
            ball.vel.x += -ball.vel.x * .05
            ball.vel.y += ball.vel.y * 0.05
        else:
            drag = 1.01

        
           

        
        
        # Set the arrow color
        if forceMagnitude == -1:
            c_arrowColor = c_white
        elif forceMagnitude < 250:
            c_arrowColor = c_blue
        elif forceMagnitude < 450:
            c_arrowColor = c_orange
        else:
            c_arrowColor = c_red

        
        polygonVerticeList = (originPos, vert1, vert2, mPos, vert3, vert4)
        #pygame.draw.polygon(screen1, c_arrowColor, polygonVerticeList, 0)

        #DRAW POLYGON AND DO CONTACTS
        for i in objects:
            i.draw(screen1)
            i.integrate(0.1)
                 
        contacts = []
        for g in contact_generators:
            contacts.extend(g.contact_all())
        
        for c in contacts:
            c.resolve()


        clock.tick(60)
        pygame.display.flip()
    

    pygame.quit()

if __name__ == "__main__":
    try: 
        main()
    except:
        pygame.quit()
        raise
            
