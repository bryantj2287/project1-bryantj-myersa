# -*- coding: utf-8 -*-
"""
Created on Mon Feb  4 14:45:45 2019

@author: Justin and Alex
"""

import pygame
from random import randint

# Commit and push from vs code
# Start of physics game
# Rules of 21 game
"""
    Start with 21 marbles on a game screen
    Player's can choose either one, two, or three marbles
    Player's alternate turns picking marbles
    Player to take last marble loses
"""
# Set up variables used for the game
def main():
    pygame.init()
    pygame.mixer.init()
    pickUp = pygame.mixer.Sound("D:\Student Data\Documents\project1-bryantj-myersa\Snd_Files\snd1.wav")
    track = pygame.mixer.Sound("D:\Student Data\Documents\project1-bryantj-myersa\Snd_Files\Tick_Tock.wav")
    loss = pygame.mixer.Sound("D:\Student Data\Documents\project1-bryantj-myersa\Snd_Files\Concussive_Hit_Guitar_Boing.wav")
    screen_width = 700
    screen_height = 300
    screen = pygame.display.set_mode([screen_width,screen_height + 250])
    
    # Vars used for checking player color
    player1 = True
    player2 = False
    p1Color = (255, 0, 0)
    p2Color = (0, 255, 0)

    # Play ambient
    track.play(1, 0, 8)
    track.set_volume(0.3)
    
    marbles_list = []
    marbles_num = 0
    while marbles_num < 21:
        marble_x = randint(0,screen_width - 50)
        marble_y = randint(0,screen_height - 50)
        num = 0
        while num < len(marbles_list):
            if marble_x > marbles_list[num].pos_x - 35 and marble_x < marbles_list[num].pos_x + 70:
                if marble_y > marbles_list[num].pos_y - 35 and marble_y < marbles_list[num].pos_y + 70:
                    num = 0
                    marble_x = randint(0,screen_width - 50)
                    marble_y = randint(0,screen_height - 50)
                else:
                    num += 1
            else:
                num += 1
                
        marbles_list.append(Marble(marble_x,marble_y,1))
        marbles_num += 1
    
    GREY = (100,100,100)
    LIGHT_GREY = (150,150,150)
    
    done = False
    
    clock = pygame.time.Clock()
    
    # Add fonts
    pygame.font.init()
    mainFont = pygame.font.SysFont("Comic Sans MS", 30)
    
    # Create surfaces to draw text onto
    txtSurface1 = mainFont.render("Player 1", False, (0, 0, 0))
    txtSurface2 = mainFont.render("Player 2", False, (0, 0, 0))
    lossTxt1 = mainFont.render("Player 1 Loses", False, (0, 0, 0))
    lossTxt2 = mainFont.render("Player 2 Loses", False, (0, 0, 0))
    button1 = mainFont.render("1", False, (0, 0, 0))
    button2 = mainFont.render("2", False, (0, 0, 0))
    button3 = mainFont.render("3", False, (0, 0, 0))
    
    
    while not done:
        marbleTxt = mainFont.render(str(marbles_num) + " MARBLES", False, (0,0,0))
        
        mouse_x, mouse_y = pygame.mouse.get_pos()
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
            elif event.type == pygame.MOUSEBUTTONUP:
                marble_tester = marbles_num
                marbles_num = buttons(0, mouse_x, mouse_y, marbles_list, marbles_num)
                if marbles_num <= 0:
                    break
                # Swap the current player
                if marble_tester is not marbles_num:
                    if player1:
                        pickUp.play(0)
                        player1 = False
                        player2 = True
                    elif player2:
                        pickUp.play(0)
                        player1 = True
                        player2 = False
                       

        
        for marble in marbles_list:
            if marble.visible is 2:
                marble.visible = 1

        marbles_num = buttons(2, mouse_x, mouse_y, marbles_list, marbles_num)
        
        # Fill Screen with current player color
        if player1:
            screen.fill(p1Color)
            screen.blit(txtSurface1, (screen_height, screen_height + 175))
        elif player2:
            screen.fill(p2Color)
            screen.blit(txtSurface2, (screen_height, screen_height + 175))
            
        # Check for writing loss text
        if player1 and marbles_num <= 0:
            screen.blit(lossTxt1, (screen_height, screen_height + 175))
            loss.play(0)
        elif player2 and marbles_num <= 0:
            screen.blit(lossTxt2, (screen_height, screen_height + 175))
            loss.play(0)
        
        num = 0
        while num < 21:
            marbles_list[num].draw(screen)
            num += 1
            
        pygame.draw.rect(screen, GREY, [100,350,100,100], 0)
        pygame.draw.rect(screen, GREY, [300,350,100,100], 0)
        pygame.draw.rect(screen, GREY, [500,350,100,100], 0)
        pygame.draw.rect(screen, LIGHT_GREY, [105,355,90,90], 0)
        pygame.draw.rect(screen, LIGHT_GREY, [305,355,90,90], 0)
        pygame.draw.rect(screen, LIGHT_GREY, [505,355,90,90], 0)
        # Draw numbers over buttons
        screen.blit(button1, (130, 380))
        screen.blit(button2, (330, 380))
        screen.blit(button3, (530, 380))
        
        screen.blit(marbleTxt, (25, screen_height + 175))
        
        pygame.display.flip()
        clock.tick(60)
                
    
    pygame.quit()
    
#Turned the checking of the buttons into a function  
def buttons(v, mouse_x, mouse_y, marbles_list, marbles_num):
    if(mouse_y > 350 and mouse_y < 450) and marbles_num is not 0:
        if(mouse_x > 100 and mouse_x < 200) or marbles_num is 1:
            marbles_list[marbles_num - 1].visible = v
            if v is 0:
                marbles_num -= 1
        elif(mouse_x > 300 and mouse_x < 400) or marbles_num is 2:
            marbles_list[marbles_num - 1].visible = v
            marbles_list[marbles_num - 2].visible = v
            if v is 0:
                marbles_num -= 2
        elif(mouse_x > 500 and mouse_x < 600):
            marbles_list[marbles_num - 1].visible = v
            marbles_list[marbles_num - 2].visible = v
            marbles_list[marbles_num - 3].visible = v
            if v is 0:
                marbles_num -= 3       
    return marbles_num

# Used to switch the current player    
def switch(p1, p2):
    if p1: # If the first parameter, or player 1 is true turn it off
        print ("switch is being used")
        return False
    else:
        return True




class Marble:
    def __init__(self,x,y,vis):
        self.pos_x = x
        self.pos_y = y
        self.visible = vis
        self.BLUE = (0,0,255)
        self.LITE = (135,206,250)
        self.YELLOW = (255,255,0)
        self.ORANGE = (255,165,0)

    def draw(self, screen):  
        if self.visible is 1:
            pygame.draw.ellipse(screen, self.BLUE, [self.pos_x, self.pos_y, 35, 35], 0)
            pygame.draw.ellipse(screen, self.LITE, [self.pos_x + 3, self.pos_y + 3, 15, 15], 0)
        elif self.visible is 2:
            pygame.draw.ellipse(screen, self.YELLOW, [self.pos_x, self.pos_y, 35, 35], 0)
            pygame.draw.ellipse(screen, self.ORANGE, [self.pos_x + 3, self.pos_y + 3, 15, 15], 0)
            
if __name__ == "__main__":
    try: 
        main()
    except:
        pygame.quit()
        raise
