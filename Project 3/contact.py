# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 11:14:47 2019

@author: sinkovitsd
"""
from vector2 import vector2
from particle import Circle
from math import sqrt
from particle import Wall
from polygon import Polygon

def sgn(x):
    if x > 0:
        return 1
    if x < 0:
        return -1
    return 0

class Contact:
    def __init__(self, obj1, obj2, data, normal, penetration, pos = None, wall_pos = None):
        self.obj1 = obj1
        self.obj2 = obj2
        self.data = data
        self.normal = normal
        self.penetration = penetration
        self.pos = pos
        self.wall_pos = wall_pos
        
    def resolve(self):
        #self.resolve_penetration()
        #self.resolve_velocity()
        if self.pos is not None and self.wall_pos is not None:
            self.penetration = (self.wall_pos - self.pos)*self.normal
        restitution = self.data['restitution']
        friction = self.data['friction']
        static = self.data['static']
        
        # Resolve penetration
        if self.penetration > 0:
            total_invmass = self.obj1.invmass + self.obj2.invmass
            if total_invmass > 0:
                nudge = self.penetration/total_invmass
                self.obj1.translate(self.obj1.invmass*nudge*self.normal)
                self.obj2.translate(-self.obj2.invmass*nudge*self.normal)
                
        # Resolve velocity
        vel1 = vector2(self.obj1.vel)
        vel2 = vector2(self.obj2.vel)
        ''' Modify vel1 and vel2 or sep_vel to take into account rotation.  
            sep_vel refers to the relative velocity at the point of contact.
            Use if hasattr(self.obj1, "angvel") to check if rotatable.'''
        sep_vel = (vel1 - vel2)*self.normal
        if(hasattr(self.obj1, "angular_v")):
            s1n = (self.pos - self.obj1.pos) % self.normal
            sep_vel += self.obj1.angular_v * s1n
        if(hasattr(self.obj2, "angular_v")):
            s2n = (self.pos - self.obj2.pos) % self.normal
            sep_vel -= self.obj2.angular_v * s2n
        if sep_vel < 0: # if moving toward one another
            if static == 0: 
                # Collisions WITHOUT friction
                target_sep_vel = -restitution*sep_vel
                delta_vel = target_sep_vel - sep_vel
                total_invmass = self.obj1.invmass + self.obj2.invmass
                ''' Add to total_invmass those extra terms involving rotation:
                    invmomi * (s % normal)**2 
                    Define s1 and s2 for use here and later.
                    Use if hasattr(self.obj, "invmomi") to check if rotatable.'''
                if(hasattr(self.obj1, "angular_v")):
                    total_invmass += self.obj1.invmom * s1n * s1n
                if(hasattr(self.obj2, "angular_v")):
                    total_invmass += self.obj2.invmom * s2n * s2n
                if total_invmass > 0:
                    impulse = delta_vel/total_invmass*self.normal
                    self.obj1.add_impulse(impulse, self.pos)
                    self.obj2.add_impulse(-impulse, self.pos)
                    return impulse # returns impulse for visualization purposes
            else: 
                # Collisions WITH friction
                # Abbreviations for normal and tangential
                n = self.normal
                t = n.perp()

                # Initial relative velocity and normal and tangential components
                vi = vel1 - vel2
                vin = vi*n
                vit = vi*t
                
                # This gets started the matrix W
                total_invmass = self.obj1.invmass + self.obj2.invmass               
                Wtt = total_invmass
                Wnn = total_invmass
                Wnt = 0
                ''' (1) Replace 0s with correct values.  
                    Define s1n, s1t, s2n, s2t, to simplify the expressions.'''
                s1n = (self.pos - self.obj1.pos) * n
                s2n = (self.pos - self.obj2.pos) * n
                s1t = (self.pos - self.obj1.pos) * t
                s2t = (self.pos - self.obj2.pos) * t
                
                if hasattr(self.obj1, "invmom"):
                    Wtt += self.obj1.invmass + self.obj1.invmom * s1t**2
                    Wnn += self.obj1.invmass + self.obj1.invmom * s1n**2
                    Wnt += self.obj1.invmom * s1n * s1t
                if hasattr(self.obj2, "invmom"):
                    Wtt += self.obj2.invmass + self.obj2.invmom * s2t**2
                    Wnn += self.obj2.invmass + self.obj2.invmom * s2n**2
                    Wnt += self.obj2.invmom * s2n * s2t
                
                # Target velocity change for perfect static friction
                dvn = -(1 + restitution)*vin
                dvt = -vit
                
                Qn = (Wnn * dvn) + (Wnt * dvt)
                Qt = (Wnt * dvn) + (Wtt * dvt)
                det = (Wnn * Wtt) - Wnt**2
                ''' (2) Compute the normal component of impulse, Jn, needed 
                    to achieve the normal velocity change, dvn. '''
                Jn = Qn / det
                ''' (3) Compute mu as friction coefficient needed 
                    to achieve the required dvt, where Jt = mu*Jn. '''
                
                ''' (4) If mu is too large for the friction coefficients, 
                    reduce it to an acceptable value. '''
                if Jn != 0:
                    mu = Qt / Qn
                    if abs(mu) > static:
                        mu = float(friction)
                        Jn = dvn / (Wtt - (mu * Wnt))
                    if abs(mu) < -static:
                        mu = float(-1 * friction)
                        Jn = dvn / (Wtt - (mu * Wnt))
                else:
                    mu = 0
                    
                dkt = mu * Jn * (vit + (0.5 * dvt))
                if dkt > 0:
                    mu = 0
                    Jn = dvn / (Wtt - (mu * Wnt))
                ''' (5) Compute impulse as the sum of normal and tangential 
                    components, Jn and Jt. '''
                Jt = mu * Jn
                impulse = (Jn + Jt) * self.normal
                
                self.obj1.add_impulse(impulse, self.pos)
                self.obj2.add_impulse(-impulse, self.pos)
                return impulse # returned so that it can be visualized"""
                
        
    def resolve_penetration(self):
        if self.penetration > 0:
            total_invmass = self.obj1.invmass + self.obj2.invmass
            nudge = self.penetration/total_invmass
            self.obj1.pos += (self.obj1.invmass*nudge)*self.normal
            self.obj2.pos -= (self.obj2.invmass*nudge)*self.normal

    def resolve_velocity(self):
        sep_vel = (self.obj1.vel - self.obj2.vel)*self.normal
        if(hasattr(self.obj1, "angular_v")):
            s1n = (self.pos - self.obj1.pos) % self.normal
            sep_vel += self.obj1.angular_v * s1n
        if(hasattr(self.obj2, "angular_v")):
            s2n = (self.pos - self.obj2.pos) % self.normal
            sep_vel += self.obj2.angular_v * s2n
        if sep_vel < 0:
            target_sep_vel = -self.restitution*sep_vel
            delta_vel = target_sep_vel - sep_vel
            total_invmass = self.obj1.invmass + self.obj2.invmass
            ''' Add to total_invmass those extra terms involving rotation:
                invmomi * (s % normal)**2 '''
            if(hasattr(self.obj1, "angular_v")):
                total_invmass += self.obj1.invmom * s1n * s1n
            if(hasattr(self.obj2, "angular_v")):
                total_invmass += self.obj2.invmom * s2n * s2n
            if total_invmass > 0:
                impulse = delta_vel/total_invmass
                self.obj1.add_impulse(impulse * self.normal, self.pos)
                self.obj2.add_impulse(-impulse * self.normal, self.pos)

class ContactGenerator:
    def __init__(self, objects, restitution=None, friction=None, static=None):#restitution=0, friction=0, static=None):
        self.objects = objects
        if restitution is None:
            restitution = 0
            print("Restitution not given.  Assuming zero restitution.")
        if friction is None:
            friction = 0
            print("Kinetic friction coefficient not given.  Assuming zero.")
        else:
            if static is None:
                static = friction
                print("Static friction coefficient not given.  Assuming the same as kinetic.")
            else:
                if static < friction:
                    print("Warning, static friction coefficient must not be less than kinetic; setting it equal to kinetic.")
                    static = friction
        self.data = {'restitution': restitution,
                     'friction': friction,
                     'static': static}
        for o in objects:
            o.contacts.append(self)
        self.contacts = []
    
    def remove(self, obj):
        self.objects.remove(obj)
        
    def contact_all(self):
        self.contacts.clear()
        # Loop through all pairs once
        for i in range(1, len(self.objects)):
            obj1 = self.objects[i]
            for j in range(i):
                obj2 = self.objects[j]
                if isinstance(obj1, Circle) and isinstance(obj2, Circle):
                    self.contacts.extend(contact_circle_circle(obj1, obj2, self.data))
                elif isinstance(obj1, Circle) and isinstance(obj2, Wall):
                    self.contacts.extend(contact_circle_wall(obj1,obj2, self.data))
                elif isinstance(obj1, Wall) and isinstance(obj2, Circle):
                    self.contacts.extend(contact_circle_wall(obj2,obj1, self.data))
                elif isinstance(obj1, Wall) and isinstance(obj2, Wall):
                    pass
                elif isinstance(obj1, Polygon) and isinstance(obj2, Circle):
                    self.contacts.extend(contact_circle_polygon(obj2,obj1, self.data))
                elif isinstance(obj1, Circle) and isinstance(obj2, Polygon):
                    self.contacts.extend(contact_circle_polygon(obj1,obj2, self.data))
                elif isinstance(obj1, Polygon) and isinstance(obj2, Polygon):
                    self.contacts.extend(contact_polygon_polygon(obj1,obj2,self.data))
                elif isinstance(obj1, Wall) and isinstance(obj2, Polygon):
                    self.contacts.extend(contact_wall_polygon(obj1,obj2,self.data))
                elif isinstance(obj1, Polygon) and isinstance(obj2, Wall):
                    self.contacts.extend(contact_wall_polygon(obj2,obj1,self.data))
                else:
                    pass
                    #print("Warning! ContactGenerator not implemented between ",
                          #type(obj1)," and ", type(obj2),".")
        return self.contacts

def contact_circle_circle(obj1, obj2, data):
    r = obj1.pos - obj2.pos
    rmag2 = r.mag2()
    R = obj1.radius + obj2.radius
    if rmag2 < R*R:
        rmag = sqrt(rmag2)
        penetration = R - rmag
        normal = r/rmag
        return [Contact(obj1, obj2, data, normal, penetration)]
    return []
        
def contact_circle_wall(circle, wall, data):
    penetration = (wall.pos - circle.pos) * wall.normal.hat() + circle.radius
    if penetration > 0:
        return [Contact(circle,wall, data, wall.normal, penetration)]
    return []
        
def contact_circle_polygon(circle, polygon, data):
    polygon_contacts = []
    is_collision = True
    for w in polygon.walls:
        penetration = (w.pos - circle.pos) * w.normal.hat() + circle.radius
        polygon_contacts.append(Contact(circle,w, data, w.normal, (w.pos - circle.pos) * w.normal.hat() + circle.radius))
        if penetration <= 0:
            is_collision = False
    
    if is_collision:
        shortest_point = polygon.points[0]
        for p in polygon.points:
            if sqrt(pow(p.y - circle.pos.y, 2) + pow(p.x - circle.pos.x, 2)) < sqrt(pow(shortest_point.y - circle.pos.y, 2) + pow(shortest_point.x - circle.pos.x, 2)):
                shortest_point = p
        
        temp_normal = (shortest_point - circle.pos).hat()
        temp_wall = Wall(circle.pos + (circle.radius * temp_normal),temp_normal)
        
        point_wall = polygon.points[0]
        num = 0
        wall_num = 0
        while num < len(polygon.points):
            if (temp_wall.pos - polygon.points[num]) * temp_wall.normal.hat() > (temp_wall.pos - point_wall) * temp_wall.normal.hat():
                point_wall = polygon.points[num]
                wall_num = int(num)
            num += 1
           
        if polygon_contacts[wall_num].penetration > 0:   
            best_wall = polygon_contacts[0]
            for c in polygon_contacts:
                if c.penetration < best_wall.penetration:
                    best_wall = c
            return [best_wall]
        return []
                      
def contact_polygon_polygon(polygon1, polygon2, data):
    least_p = float('inf')
    current_contact = []
    for i in range(len(polygon1.normals)):
        max_p = -float('inf')
        for r in polygon2.points:
            p = (polygon1.points[i] - r) * polygon1.normals[i]
            if p > max_p:
                max_p = p
                max_r = r
        if max_p < 0:
            return []
        if max_p < least_p:
            least_p = max_p
            least_r = max_r
            least_n = polygon1.normals[i]
            current_contact = [Contact(polygon2, polygon1, data, least_n, least_p, least_r)]
    
    for i in range(len(polygon2.normals)):
    	max_p = -float('inf')
    	for r in polygon1.points:
    		p = (polygon2.points[i] - r) * polygon2.normals[i]
    		if p > max_p:
    			max_p = p
    			max_r = r
    	if max_p < 0:
    		return []
    	if max_p < least_p:
            least_p = max_p
            least_r = max_r
            least_n = polygon2.normals[i]
            current_contact = [Contact(polygon1, polygon2, data, least_n, least_p, least_r)]
    
    return current_contact
            
            
def contact_wall_polygon(wall, polygon, data):
    least_p = float('inf')
    current_contact = []
    max_p = -float('inf')
    for r in polygon.points:
        p = (wall.pos - r) * wall.normal.hat()
        if p > max_p:
            max_p = p
            max_r = r
    if max_p < 0:
        return []
    if max_p < least_p:
        least_p = max_p
        least_r = max_r
        least_n = wall.normal.hat()
        current_contact = [Contact(polygon, wall, data, least_n, least_p, least_r)]
    return current_contact




