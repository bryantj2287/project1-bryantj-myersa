# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 14:00:43 2019

@author: sinkovitsd
"""

from rotating import RotatingParticle
from polygon import Polygon
from particle import Wall
from vector2 import vector2
from math import sin, cos, sqrt
from pointInPolygon import Side
import pygame

class UniformPolygon(RotatingParticle, Polygon):
    def __init__(self, density, pos, vel, angle, angvel, opoints, color=(0,0,0), gravity=(0,0)):
        self.color = color
        # Copy opoints
        n = len(opoints)
        self.opoints = []
        for p in opoints:
            self.opoints.append(vector2(p))
        
        # Calculate onormals
        self.onormals = []
        for i in range(n):
            self.onormals.append((self.opoints[i]-self.opoints[i-1]).perp().hat())
        
        # Check for convexity and fix onormals
        for i in range(n):
            npos = 0
            nneg = 0
            for p in self.opoints:
                d = (p - self.opoints[i])*self.onormals[i] 
                if d > 1e-13:
                    npos += 1
                if d < -1e-13:
                    nneg += 1
            if npos > 0:
                if nneg == 0:
                    self.onormals[i] *= -1
                else:
                    print("WARNING! Nonconvex polygons are not allowed.", self.onormals[i], npos, nneg)
                
        
        
        mass = 0
        momi = 0
        center_mass_total = vector2(0,0)
        # For each triangle,
        for i in range(n - 2):
            # Calculate the area of the triangle and its mass = density*area.
            s1 = vector2(opoints[i + 1] - opoints[0])
            s2 = vector2(opoints[i + 2] - opoints[0])
            A = 0.5 * abs(s1 % s2)
            mass_piece = density * A
            # Calculate the centroid (center of mass) of the triangle.
            R = (s1 + s2) / 3
            # Calculate the moment of inertia of the triangle.
            I = (mass_piece / 6) * (s1.mag2() + s2.mag2() + (s1 * s2)) 
            # Add the mass*centroid to the center-of-mass total.
            center_mass_total += mass_piece * R
            # Add the moment of intertia to the moment of inertia total. (momi)
            momi += I
            # Add the mass to the mass total. (mass)
            mass += mass_piece
        # Outside of for loop now.
        # Calculate the center of mass (centroid) of the polygon 
        # by dividing the center-of-mass total by the total mass.
        centroid = center_mass_total / mass
        # Calculate the moment of inertia about the center of mass (momi)
        # by subtracting the total mass times the magnitude of the centroid squared.
        momi -= mass * centroid.mag2()
        # Shift the opoints to be about the new center of mass by
        # subtracting the centroid vector from opoint.
        for point in opoints:
            point -= centroid
        # Shift the position to account for the previous step 
        # by adding the centroid vector, rotated by angle, to the original position.
        # (The rotation is necessary to account for any nonzer angle given.)
        pos += centroid.rotated(angle)
                
        # Now we call __init__ for the RotatingParticle superclass.
        super().__init__(mass, pos, vel, gravity, momi, angle, angvel)

        # Create blank points, normals, and drawpoints, then update them
        self.points = []
        self.normals = []
        self.drawpoints = []
        
        for i in range(n):
            self.points.append(vector2(0,0))
            self.normals.append(vector2(0,0))
            self.drawpoints.append((0,0))
            
        self.walls = []
        num = 0
        while num < len(self.points): 
            #pos, normal, mass = 0, vel = vector2(0,0), gravity = vector2(0,0), color = (0,0,0), length=10000
            self.walls.append(Wall((self.points[num - 1]+self.points[num])/2, #Position is the midpoint of the two points
                                   self.normals[num], #Normal is the vector perpendicular to the points 
                                   0,
                                   vector2(0,0), 
                                   vector2(0,0), 
                                   (255,255,255), 
                                   sqrt(pow(self.points[num].y - self.points[num - 1].y, 2) + pow(self.points[num].x - self.points[num - 1].x, 2)) / 2)) #Length is half the distance between the two points
            num += 1
            
        self.update_points_normals()
        
        self.sides = []
        n = len(self.points)
        for i in range(n):
            self.sides.append(Side(self.points[i - 1], self.points[i]))
        
    def integrate(self, dt):
        super().integrate(dt)
        self.update_points_normals()
    
    def update_points_normals(self):
        cosangle = cos(self.angle)
        sinangle = sin(self.angle)
        for i in range(len(self.opoints)):
            # rotate point
            self.points[i].assign(self.opoints[i].rotated(sinangle, cosangle))
            # translate point
            self.points[i] += self.pos
            # set drawpoint as integer
            self.drawpoints[i] = self.points[i].pygame()
            # rotate normal
            self.normals[i].assign(self.onormals[i].rotated(sinangle, cosangle))
            
        for i in range(len(self.opoints)):
            self.walls[i].normal = self.normals[i]
            self.walls[i].pos = (self.points[i - 1]+self.points[i])/2

        self.sides = []
        n = len(self.points)
        for i in range(n):
            self.sides.append(Side(self.points[i - 1], self.points[i]))
            
            
    
    def draw(self, screen):
        pygame.draw.polygon(screen, self.color, self.drawpoints, 0)
        pygame.draw.circle(screen, (0,0,0), self.pos.pygame(), 5)
        # draw normals
        if True:
            for i in range(len(self.points)):
                pygame.draw.line(screen, (0,0,0), self.drawpoints[i], 
                                 (self.points[i] + 50*self.normals[i]).pygame())
            n = len(self.points)
            for i in range(n):
                self.sides[i].drawSide(screen)
                
    def translate(self, disp):
        self.pos += disp
        self.update_points_normals()
                