# This code will be used to check whether or not a point is in a polygon
from vector2 import vector2
import pygame



def inPolygon(point, polygon):
    # Make point a side
    end_point = vector2(point.x + 10000, point.y)
    ray = Side(point, end_point)
    count = 0;
    for n in polygon.sides:
        if (ray.intersect(ray, n)):
            count += 1
    print(count)
    if (count % 2) == 0:
        return False
    else:
        return True

def checkWithTolerance(num1, num2, tolerance):
    # print("Low Tolerance is " + str(num2 - tolerance) + " High Tolerance is " + str(num2 + tolerance) + " Checking against " + str(num1))
    if num1 < num2 + tolerance and num1 > num2 - tolerance:
        return True
    return False
def checkVectorWithTolerance(vec1, vec2, tolerance):
    # Check if the X value is within toleance
    if checkWithTolerance(vec1.x, vec2.x, tolerance):
        # Check the Y value is within tolerance
        if checkWithTolerance(vec1.y, vec2.y, tolerance):
            # Return true, vectors are nearby
            print("Vector Tolerance Function has returned true");
            return True
    return False

class Side:
    def __init__(self, vec1, vec2):
        self.vec1 = vec1
        self.vec2 = vec2
        self.points = []
        self.iX = 0
        self.iY = 0
        # When Calculating Slope, check for potential division by zero errors
        if (self.vec2.x - self.vec1.x ) == 0:
            self.slope = 1
        else:  
            self.slope = (self.vec2.y - self.vec1.y) / (self.vec2.x - self.vec1.x)
        # Calculate the formula of the line

    def getSlope(self):
        # Check for division by zero answers
        return self.slope
    def drawSide(self, screen):
         pygame.draw.line(screen, (255, 0, 0), self.vec1, self.vec2, 5)
    def getPoints(self):
        x = self.vec1.x
        y = self.vec1.y
        while not checkWithTolerance(x, self.vec2.x, 5):
            x += 1
            y += self.slope
            self.points.append(vector2(x, y))
    def printPoints(self):
        print(self.points)
    def intersect(self, l1, l2):
        print("Intersetion called")
        # Calculate the domain of side 1 and 2
        side1_domain_low = l1.vec1.x
        side1_domain_high = l1.vec2.x
        side1_range_low = l1.vec1.y
        side1_range_high = l1.vec2.y
        side2_domain_low = l2.vec1.x
        side2_domain_high = l2.vec2.x
        side2_range_low = l2.vec1.y
        side2_range_high = l2.vec2.y
        # Order the domain and ranges such that low is smaller than high
        # Side 1 domain
        if (side1_domain_low > side1_domain_high):
            temp = side1_domain_low
            side1_domain_low = side1_domain_high
            side1_domain_high = temp
        # Side 1 range
        if (side1_range_low > side1_range_high):
            temp = side1_range_low
            side1_range_low = side1_range_high
            side1_range_high = temp
        # Side 2 Domain
        if (side2_domain_low > side2_domain_high):
            temp = side2_domain_low
            side2_domain_low = side2_domain_high
            side2_domain_high = temp
        # Side 2 Range
        if (side2_range_low > side2_range_high):
            temp = side2_range_low
            side2_range_low = side2_range_high
            side2_range_high = temp
        # Check if the two given lines will intersect
        if l1.slope != l2.slope:
            b1 =l1.vec1.y - (l1.slope * l1.vec1.x)
            b2 = l2.vec1.y - (l2.slope * l2.vec1.x)
            c = b2 - b1
            d = l1.slope - l2.slope
            iX = c / d
            print(iX)
            self.iX = iX
            iY = l1.slope * iX + b1
            print(iY)
            self.iY = iY
            # Check to see if the point is in the domain and range of both lines
            if iX <= side1_domain_high and iX >= side1_domain_low and iX <= side2_domain_high and iX >= side2_domain_low:
                print("X existed in the domain")
                if iY <= side1_range_high and iY >= side1_range_low and iY <= side2_range_high and iY >= side2_range_low:
                    print("Y existed in the domain")
                    return True
        return False
    def drawIntersects(self, screen, iX, iY):
        iPoint = vector2(iX, iY)
        pygame.draw.circle(screen, (0, 0, 255), iPoint, 25, 0)


side1 = Side(vector2(-2.45, -4.2), vector2(1.7, -0.15))
side2 = Side(vector2(0, 0), vector2(1, -2))
side1.getPoints()
side2.getPoints()
if side1.intersect(side1, side2):
    print("Intersection Found")
else:
    print("Intersection not found")