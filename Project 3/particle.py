# -*- coding: utf-8 -*-
"""
Created on Wed Feb 20 14:36:04 2019

@author: Student
"""

from vector2 import vector2
import pygame

class Particle:
    def __init__(self, mass, pos, vel=vector2(0,0), gravity=vector2(0,0)):
        if mass == 0:
            self.invmass = 0
        else:
            self.invmass = 1.0/mass
        self.pos = vector2(pos)
        self.vel = vector2(vel)
        self.gforce = mass*vector2(gravity)
        self.force = self.gforce
        self.interactions = []
        self.contacts = []
        
    def integrate(self, dt):
        self.vel += self.invmass*self.force*dt
        self.pos += self.vel*dt
        self.force = vector2(self.gforce)

    def remove(self):
        for i in self.interactions:
            i.remove(self)
    
    def add_impulse(self, impulse, pos=None):
        self.vel += self.invmass*impulse
        
    def translate(self, disp):
        self.pos += disp
        
class Circle(Particle):
    def __init__(self,mass,pos,vel,gravity,radius,color):
        Particle.__init__(self,mass,pos,vel,gravity)
        self.radius = radius
        self.color = color

    def draw(self, screen):
        pygame.draw.circle(screen,self.color,[round(self.pos.x), round(self.pos.y)],int(self.radius))
        
class Wall(Particle):
    def __init__(self, pos, normal, mass = 0, vel = vector2(0,0), gravity = vector2(0,0), color = (0,0,0), length=10000):
        Particle.__init__(self,mass,pos,vel,gravity)
        self.normal = normal.hat()
        self.color = color
        self.length = length
        self.sides = []
        
        
    def draw(self, screen):
        tangent = self.normal.perp()
        p1 = (self.pos + self.length*tangent).pygame()
        p2 = (self.pos + -1*self.length*tangent).pygame()
        pygame.draw.line(screen, self.color, p1, p2, 1)
        pygame.draw.line(screen, (0,255,0), self.pos, (self.pos + (self.normal * 10)))
        