# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 14:28:21 2019

@author: sinkovitsd
"""

import pygame
from random import randint, uniform, shuffle
from math import pi, sin, cos
from uniform import UniformPolygon
from particle import Wall, Circle
from vector2 import vector2
import contact
import colorsys
from polygon import Polygon
from pointInPolygon import inPolygon

WHITE = (255,255,255)
BLACK = (0,0,0)

def random_hsv_color(hlo, hhi, slo, shi, vlo, vhi):
    if hhi <  hlo:
        hhi += 1
    h = uniform(hlo, hhi)%1
    s = uniform(slo, shi)
    v = uniform(vlo, vhi)
    r, g, b = colorsys.hsv_to_rgb(h,s,v)
    return int(255*r), int(255*g), int(255*b)

def main():
    pygame.init()
    screen = pygame.display.set_mode([800,600])
    
    gravity = vector2(0,200) # Downward uniform gravity, set to zero
    
    # list of objects in world
    objects = []
    # list of forces active    
    forces = []
    # list of contact generators
    contact_generators = []
    # list of contacts to be resolved
    contacts = []
    
    #density, pos, vel, angle, angvel, opoints, color=(0,0,0), gravity=(0,0)
    
    #objects.append(Circle(9, vector2(400-30,300), vector2(0,1), 60, (255,127,0), gravity))
    for n in range(3):
        objects.append(UniformPolygon(1e-4, vector2(randint(100,700),randint(0,500)), 
                                      vector2(0,0), uniform(-pi,pi), 0, 
                                      
                                      [vector2(randint(20,40), randint(-60,-40)),
                                       vector2(randint(-40,-20), randint(-60,-40)),
                                       vector2(randint(-10,10), randint(40,60))],
                                      (randint(0,255),randint(0,255),randint(0,255)), gravity))
        
    objects.append(UniformPolygon(1e-4, vector2(300,400), vector2(0,0), 0,0, [vector2(-40,40),vector2(40,40),vector2(40,-40), vector2(-40,-40)], (randint(0,255),randint(0,255),randint(0,255)), gravity))
    objects.append(UniformPolygon(1e-4, vector2(400,300), vector2(0,0), 0,0, [vector2(-30,30),vector2(30,30),vector2(30,-30), vector2(-30,-30)], (randint(0,255),randint(0,255),randint(0,255)), gravity))
    objects.append(UniformPolygon(1e-4, vector2(200,100), vector2(0,0), 0,0, [vector2(-20,20),vector2(20,20),vector2(20,-20), vector2(-20,-20)], (randint(0,255),randint(0,255),randint(0,255)), gravity))
    #print(1/objects[0].invmass, 1/objects[0].invmomi) 
    
    objects.append(Wall(vector2(0,600), vector2(0,-1)))
    objects.append(Wall(vector2(200,400), vector2(1,-1)))
    objects.append(Wall(vector2(600,500), vector2(-1,-2)))
    #objects.append(Wall(vector2(200,400), vector2(0,-1)))
    contact_generators.append(contact.ContactGenerator(objects, 0, 0.2, 0.3))

    # Main Loop
    done = False
    frame_rate = 60
    dt = 1.0/frame_rate

    # User Interaction Stuff
    objIndex = -1
    objFound = False
    pygame.key.set_repeat(1)
    storedVel = vector2(0, 0)
    notStored = True
    oriMpos = vector2(0, 0)
    throwForce = 150

    #Line draw stuff
    mPos = vector2(0, 0)
    clock = pygame.time.Clock()
    while not done:
        # --- Main event loop
        for event in pygame.event.get(): # User did something
            if (event.type == pygame.QUIT # If user clicked close
                or (event.type == pygame.KEYDOWN 
                    and event.key == pygame.K_ESCAPE)): # or pressed ESC
                done = True # Flag that we are done so we exit this loop
            if (event.type == pygame.MOUSEBUTTONDOWN):
                xPos, yPos = pygame.mouse.get_pos()
                mPos = vector2(0,0)
                mPos.x = xPos
                mPos.y = yPos
                numObject = len(objects)
                for i in range(numObject):
                    if inPolygon(mPos, objects[i]):
                        print("InPolygon working");
                        # Store the index of the object clicked
                        objIndex = i
                        # Tell the code that an object was found, used later
                        objFound = True
            if (event.type == pygame.MOUSEBUTTONUP):
                if (objFound):
                    # Calculate the throw -> Horizontal Pull
                    if (oriMpos.x > mPos.x):
                        # Leftward pull
                        storedVel.x += throwForce
                    elif (oriMpos.x < mPos.x):
                        # Rightware Pull
                        storedVel.x -= throwForce
                    # Calulate Throw -> Vertical Pull
                    if (oriMpos.y > mPos.y):
                        # Downward
                        storedVel.y -= throwForce
                    elif (oriMpos.y < mPos.y):
                        # Upward
                        storedVel.y += throwForce
                    print(storedVel)
                    objects[objIndex].vel = storedVel
                notStored = False
                 # If there is a found object unflag it
                objFound = False
            if (event.type == pygame.KEYDOWN and event.key == pygame.K_a):
                # Check if an object is held
                if (objFound):
                    # Set angle to += an amount
                    objects[objIndex].angle -= 0.1
            if (event.type == pygame.KEYDOWN and event.key == pygame.K_d):
                # Check if an object is held
                if (objFound):
                    # Set angle to += an amount
                    objects[objIndex].angle += 0.1

                
        
        # Add forces
        for f in forces:
            f.force_all()
        
        # Move objects
        for o in objects:
            o.integrate(dt)
        
        # Get contacts
        niterations = 0
        max_iterations = 1
        while niterations < max_iterations:
            niterations += 1
            contacts = []
            for g in contact_generators:
                contacts.extend(g.contact_all())
              
            if len(contacts)==0:
                break
                
            # Resolve contacts
            contacts.sort(key=lambda x: x.penetration)
            for c in contacts:
                c.resolve()

        # Draw objects to screen
        screen.fill(WHITE) # clears the screen
        for o in objects:
            #print(o.pos)
            o.draw(screen)

        # Draw Line
        pygame.draw.line(screen, (255, 0, 0), mPos, vector2(mPos.x + 1000, mPos.y), 1)
        # Draw intersect points

        # User Interaction with a select object
        if objFound:
            # Collect Mouse Position
            xPos, yPos = pygame.mouse.get_pos()
            mPos = vector2(0,0)
            mPos.x = xPos
            mPos.y = yPos
            if (notStored):
                oriMpos = mPos
                notStored = False
            
            xPos, yPos = pygame.mouse.get_pos()
            mPos = vector2(0,0)
            mPos.x = xPos
            mPos.y = yPos
            # Store the previous velocity
            storedVel = objects[objIndex].vel
            # Set Position
            objects[objIndex].pos = mPos
            # Set Angular Velocity
            objects[objIndex].angular_v = 0
            # Lock Velocity
            objects[objIndex].vel = vector2(0, 0)
            
        
        # Update the screen
        pygame.display.update()

        # --- Limit to 60 frames per second
        clock.tick(60)
        
    pygame.quit()
 
""" Safe start """
if __name__ == "__main__":
    try:
        main()
    except:
        pygame.quit()
        raise