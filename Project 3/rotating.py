# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 14:46:13 2019
"""
from particle import Particle
from math import sin, cos
import pygame
from vector2 import vector2

class RotatingParticle(Particle):
    def __init__ (self, mass, pos, vel, gravity, moment_of_inertia, angle, angular_velocity):
        Particle.__init__(self,mass,pos,vel,gravity)
        if moment_of_inertia == 0:
            self.invmom = 0
        else:
            self.invmom = 1.0/moment_of_inertia
        self.angle = angle
        self.angular_v = angular_velocity
        self.torque = 0
        
    def integrate(self, dt):
        Particle.integrate(self,dt)
        self.angular_v += self.invmom * self.torque * dt
        self.angle += self.angular_v * dt 
        self.torque = 0
        
    def add_force(self, force, pos = None):
        self.force += force
        if pos is not None:
            r = pos - self.pos
            self.torque += r % force
            
    def add_impulse(self, impulse, pos=None):
        #print("impulse", self.vel, self.angvel)
        self.vel += self.invmass*impulse
        if pos is not None:
            r = pos - self.pos
            self.angular_v += self.invmom*(r % impulse)
            #print("impulse", self.invmass*impulse, self.invmomi*(r % impulse), r)
            #print("impulse", self.vel, self.angvel)
        
            
            
            
class RotatingPolygon(RotatingParticle):
    def __init__ (self, mass, pos, vel, gravity, moment_of_inertia, angle, angular_velocity, opoints):
        RotatingParticle.__init__(self,mass,pos,vel,gravity, moment_of_inertia, angle, angular_velocity)
        self.opoints = []
        for p in opoints:
            self.opoints.append(vector2(p))
        self.onormals = []

        num = 0
        while num < len(self.opoints):
            self.onormals.append((self.opoints[num - 1] - self.opoints[num]).perp().hat())
            num += 1
            
        self.points = []
        for o in self.opoints:
            self.points.append(o + self.pos)
        self.normals = []
        for n in self.onormals:
            self.normals.append(n)
        
    def integrate(self, dt):
        RotatingParticle.integrate(self, dt)
        self.update()
            
    def update(self):
        i = 0
        while i < len(self.points):
            self.points[i].x = self.opoints[i].x * cos(self.angle) - self.opoints[i].y * sin(self.angle) + self.pos.x
            self.points[i].y = self.opoints[i].x * sin(self.angle) + self.opoints[i].y * cos(self.angle) + self.pos.y
            self.normals[i].x = self.onormals[i].x * cos(self.angle) - self.onormals[i].y * sin(self.angle)
            self.normals[i].y = self.onormals[i].x * sin(self.angle) + self.onormals[i].y * cos(self.angle)
            i += 1
            
    def draw(self, screen):
        pygame.draw.polygon(screen, (0,0,255), self.points)